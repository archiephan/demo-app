FROM python:3.6
RUN mkdir -p /app
WORKDIR /app

COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY . /app

EXPOSE 9090

#ENTRYPOINT [ "/usr/local/bin/gunicorn", "app:app", "-b", "0.0.0.0:9100", "-w", "4", "--log-config", "gunicorn.conf" ]

ENTRYPOINT [ "python", "app.py" ]
